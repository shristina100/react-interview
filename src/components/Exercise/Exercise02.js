import React, { useState } from "react";

function Exercise02() {
  let [name, setName] = useState("");
  let [fileName, setFileName] = useState("");

  let handleChange = (e) => {
    e.preventDefault();
    setFileName(e.target.files[0]);
  };

  let handleSubmit = (e) => {
    e.preventDefault();
    axios.post("/", { name: name, avatar: fileName });
  };
  return (
    <div>
      <h1>Add Category</h1>
      <form action="" encType="multipart/form-data">
        <div className="form-group">
          <label htmlFor="">Category Name</label>
          <input
            type="text"
            name="name"
            className="form-control"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label htmlFor="">Category Image</label>
          <input
            type="file"
            name="image"
            className="form-control"
            onLoad={(e) => handleChange()}
          />
        </div>

        <button className="btn btn-primary">Submit</button>
      </form>
    </div>
  );
}

export default Exercise02;
