import React, { useState } from "react";

function Exercise03() {
  let [formData, setFormData] = useState({
    schoolName: "",
    location: "",
    grade: "",
    section: "",
    rank: "",
    profile: null,
  });

  let { schoolName, location, grade, section, rank, profile } = formData;

  let handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: [e.target.value] });
  };

  let handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData);
  };

  return (
    <div>
      <h1>Add Category</h1>
      <form
        action=""
        encType="multipart/form-data"
        onSubmit={(e) => handleSubmit(e)}
      >
        <div className="form-group">
          <label htmlFor="">School Name</label>
          <input
            type="text"
            name="schoolName"
            className="form-control"
            value={schoolName}
            onChange={(e) => handleChange(e)}
          />
        </div>

        <div className="form-group">
          <label htmlFor="">Location</label>
          <input
            type="text"
            name="location"
            className="form-control"
            value={location}
            onChange={(e) => handleChange(e)}
          />
        </div>

        <div className="form-group">
          <label htmlFor="">Grade</label>
          <input
            type="text"
            name="grade"
            className="form-control"
            value={grade}
            onChange={(e) => handleChange(e)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">Section</label>
          <input
            type="text"
            name="section"
            className="form-control"
            value={section}
            onChange={(e) => handleChange(e)}
          />
        </div>

        <div className="form-group">
          <label htmlFor="">Rank</label>
          <input
            type="text"
            name="rank"
            value={rank}
            className="form-control"
            onChange={(e) => handleChange(e)}
          />
        </div>

        <div className="form-group">
          <label htmlFor="">Profile</label>
          <input
            type="file"
            name="profile"
            value={profile}
            className="form-control"
            onChange={(e) =>
              setFormData({ ...formData, [e.target.name]: e.target.files[0] })
            }
          />
        </div>

        <button className="btn btn-primary">Submit</button>
      </form>
    </div>
  );
}

export default Exercise03;
