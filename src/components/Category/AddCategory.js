import React from "react";
import { Formik } from "formik";
import axios from "axios";
export default function AddCategory() {
  return (
    <div>
      <h1>Add Category</h1>
      <Formik
        initialValues={{
          name: "",
          avatar: undefined,
        }}
        onSubmit={(props) => {
          let formData = new FormData();
          formData.append("name", props.name);
          formData.append("avatar", props.avatar);
          let config = {
            method: "post",
            url: "http://localhost:5000/api/qa/",

            data: formData,
          };

          axios(config).then((res) => console.log(res.data));
        }}
        validate={(props) => {
          let errors = {};

          if (!props.name) {
            errors.name = "Required";
          }
          if (!props.avatar) {
            errors.avatar = "Required";
          }
          console.log(errors);
          return errors;
        }}
      >
        {(props) => {
          //console.log(props)
          console.log(props.values);
          let { values, handleSubmit, handleChange, setFieldValue } = props;
          let { name } = values;
          return (
            <form encType="multipart/form-data" onSubmit={handleSubmit}>
              <div className="form-group">
                <label htmlFor="">Name</label>
                <input
                  type="text"
                  name="name"
                  value={name}
                  onChange={handleChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="">Avatar</label>
                <input
                  type="file"
                  name="avatar"
                  onChange={(e) => setFieldValue("profile", e.target.files[0])}
                />
              </div>
              <button className="btn btn-primary">Submit</button>
            </form>
          );
        }}
      </Formik>
    </div>
  );
}
