import React, { useState, Fragment, useEffect } from "react";
import "./App.css";
import AddCategory from "./components/Category/AddCategory";
import axios from "axios";

function App() {
  let [categories, setCategories] = useState([null]);
  let [loading, setLoading] = useState(false);
  useEffect(() => {
    let res = axios.get("http://localhost:5000/api/qa").then((res) => {
      setCategories(res.data);
      setLoading(false);
    });
  }, []);
  return (
    <Fragment>
      {categories.length == 0 ? <AddCategory /> : <AddCategory />}
    </Fragment>
  );
}

export default App;
